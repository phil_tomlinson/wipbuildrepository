package buildRepository.unittest;

import canonicalDataModel.V1.common.ApplicationInformation;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * TDigital Ventures
 * Date: 14/10/13
 * Time: 7:34 PM
 */
@Configuration
public class ApplicationInformationQueryServiceConfiguration {

    final static public String buildNumber = "1";
    final static public String artifactName="BuildRepository";
    final static public String artifactVersion = "1.0.0";

    @Bean
    public ApplicationInformation getApplicationInformation() {
        ApplicationInformation appInfo = new ApplicationInformation();
        appInfo.setBuildNumber(buildNumber);
        appInfo.setArtifactName(artifactName);
        appInfo.setArtifactVersion(artifactVersion);
        return appInfo;
    }
}
