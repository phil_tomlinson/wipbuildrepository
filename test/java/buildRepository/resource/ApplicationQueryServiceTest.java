package buildRepository.resource;

import buildRepository.unittest.ApplicationInformationQueryServiceConfiguration;
import canonicalDataModel.V1.common.ApplicationInformation;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.inject.Inject;

/**
 * TDigital Ventures
 * Date: 14/10/13
 * Time: 12:35 PM
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:unittest-context.xml")
public class ApplicationQueryServiceTest {

    private ApplicationQueryServiceResource applicationQueryService;

    @Test
    public void testApplicationQueryService() {
        String appInfo = getApplicationQueryService().getApplicationInformation();
        Assert.assertNotNull(appInfo);
        Assert.assertTrue(appInfo.contains(ApplicationInformationQueryServiceConfiguration.buildNumber));
        Assert.assertTrue(appInfo.contains(ApplicationInformationQueryServiceConfiguration.artifactName));
        Assert.assertTrue(appInfo.contains(ApplicationInformationQueryServiceConfiguration.artifactVersion));
    }

    public ApplicationQueryServiceResource getApplicationQueryService() {
        return applicationQueryService;
    }

    @Inject
    public void setApplicationQueryService(ApplicationQueryServiceResource applicationQueryService) {
        this.applicationQueryService = applicationQueryService;
    }
}
