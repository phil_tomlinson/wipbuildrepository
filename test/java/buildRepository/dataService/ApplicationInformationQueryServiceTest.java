package buildRepository.dataService;

import buildRepository.unittest.ApplicationInformationQueryServiceConfiguration;
import canonicalDataModel.V1.common.ApplicationInformation;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.inject.Inject;

/**
 * TDigital Ventures
 * Date: 14/10/13
 * Time: 12:35 PM
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:unittest-context.xml")
public class ApplicationInformationQueryServiceTest {

    private ApplicationInformationQueryServices applicationInformationQueryService;

    @Test
    public void testApplicationInformationQueryServices() {
        ApplicationInformation appInfo = applicationInformationQueryService.getApplicationInformation();
        Assert.assertNotNull(appInfo);
        Assert.assertEquals(ApplicationInformationQueryServiceConfiguration.buildNumber, appInfo.getBuildNumber());
        Assert.assertEquals(ApplicationInformationQueryServiceConfiguration.artifactName, appInfo.getArtifactName());
        Assert.assertEquals(ApplicationInformationQueryServiceConfiguration.artifactVersion, appInfo.getArtifactVersion());
    }

    public ApplicationInformationQueryServices getApplicationInformationQueryService() {
        return applicationInformationQueryService;
    }

    @Inject
    public void setApplicationInformationQueryService(ApplicationInformationQueryServices applicationInformationQueryService) {
        this.applicationInformationQueryService = applicationInformationQueryService;
    }

}
