package buildRepository.resource;

import buildRepository.businessService.ApplicationInformationQueryBusinessService;
import buildRepository.view.ApplicationMapper;
import canonicalDataModel.V1.common.ApplicationInformation;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

/**
 * TDigital Ventures
 * Date: 14/10/13
 * Time: 11:20 AM
 */
@Named
@Path("/@TAM@/V1")
public class ApplicationQueryServiceResource {

    private ApplicationInformationQueryBusinessService controller;
    private ApplicationMapper mapper;

    @GET
    @Path("/ApplicationInformation")
    public String getApplicationInformation(){
        try {
            ApplicationInformation appInfo = getController().getApplicationInformation();
            return getMapper().asJson(appInfo);
        }catch(Exception ex){
            return ex.getMessage();
        }
    }

    public ApplicationInformationQueryBusinessService getController() {
        return controller;
    }

    @Inject
    public void setController(ApplicationInformationQueryBusinessService controller) {
        this.controller = controller;
    }

    public ApplicationMapper getMapper() {
        return mapper;
    }

    @Inject
    public void setMapper(ApplicationMapper mapper) {
        this.mapper = mapper;
    }
}
