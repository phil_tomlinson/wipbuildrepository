package buildRepository.view;

import canonicalDataModel.V1.common.ApplicationInformation;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import javax.inject.Named;
import java.io.IOException;

/**
 * TDigital Ventures
 * Date: 19/09/13
 * Time: 12:09 PM
 */
@Named
public class ApplicationMapper {

    public String asJson(ApplicationInformation c) throws JsonGenerationException,JsonMappingException,IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.getSerializationConfig().addMixInAnnotations(ApplicationInformation.class, ApplicationInformationMixIn.class);
        String req = mapper.writeValueAsString(c);
        return req;
    }

    public ApplicationInformation fromJson(String json)  {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.getSerializationConfig().addMixInAnnotations(ApplicationInformation.class, ApplicationInformationMixIn.class);
            ApplicationInformation ai = mapper.readValue(json,ApplicationInformation.class);
            return ai;
        }catch(Exception ex){
            throw new RuntimeException(ex);
        }
    }
}
