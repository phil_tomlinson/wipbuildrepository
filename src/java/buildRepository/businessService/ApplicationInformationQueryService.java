package buildRepository.businessService;

import buildRepository.dataService.ApplicationInformationQueryServices;
import canonicalDataModel.V1.common.ApplicationInformation;


import javax.inject.Inject;
import javax.inject.Named;

/**
 * TDigital Ventures
 * Date: 14/10/13
 * Time: 11:29 AM
 */
@Named
public class ApplicationInformationQueryService implements ApplicationInformationQueryBusinessService {

    private ApplicationInformationQueryServices queryService;

    //@Transactional(propagation = Propagation.REQUIRED)
    public ApplicationInformation getApplicationInformation(){
         return getQueryService().getApplicationInformation();
    }

    public ApplicationInformationQueryServices getQueryService() {
        return queryService;
    }

    @Inject
    public void setQueryService(ApplicationInformationQueryServices queryService) {
        this.queryService = queryService;
    }

    //@Inject
    //public void setQueryService(@Named("enterpriseDataSource") ApplicationInformationQueryServices queryService) {
    //    this.queryService = queryService;
    //}
}
