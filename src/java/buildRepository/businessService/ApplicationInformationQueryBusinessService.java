package buildRepository.businessService;

import canonicalDataModel.V1.common.ApplicationInformation;

/**
 */
public interface ApplicationInformationQueryBusinessService {

    ApplicationInformation getApplicationInformation();

}