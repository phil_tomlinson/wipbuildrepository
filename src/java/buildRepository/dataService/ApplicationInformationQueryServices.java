package buildRepository.dataService;

import canonicalDataModel.V1.common.ApplicationInformation;

/**
 */
public interface ApplicationInformationQueryServices {

    ApplicationInformation getApplicationInformation();

}