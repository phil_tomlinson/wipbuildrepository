package buildRepository.dataService;

import canonicalDataModel.V1.common.ApplicationInformation;


import javax.inject.Inject;
import javax.inject.Named;
import java.io.InputStream;

/**
 * TDigital Ventures
 * Date: 14/10/13
 * Time: 7:28 AM
 */
@Named
public class ApplicationInformationQueryServicesImpl implements ApplicationInformationQueryServices {

    private ApplicationInformation applicationInformation;

    public ApplicationInformation getApplicationInformation(){
        return applicationInformation;
    }

    @Inject
    public void setApplicationInformation(ApplicationInformation applicationInformation) {
        this.applicationInformation = applicationInformation;
    }
}
